import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String[] fruits = new String[5];
        fruits[0] = "Banana";
        fruits[1] = "Mango";
        fruits[2] = "Apple";
        fruits[3] = "Grapes";
        fruits[4] = "Watermelon";

        System.out.println("Fruits in stock: " + Arrays.toString(fruits));
        System.out.println("Pick a fruit to see its index :)");
        Scanner scan = new Scanner(System.in);

        String fruitPicker = scan.nextLine();
        int result = Arrays.binarySearch(fruits, fruitPicker);

        switch(fruitPicker) {
            case "Banana":
                System.out.println("The index of " + fruitPicker + " is" + " " + result);
                break;
            case "Mango":
                System.out.println("The index of " + fruitPicker + " is" + " " + result);
                break;
            case "Apple":
                System.out.println("The index of " + fruitPicker + " is" + " " + result);
                break;
            case "Grapes":
                System.out.println("The index of " + fruitPicker + " is" + " " + result);
                break;
            case "Watermelon":
                System.out.println("The index of " + fruitPicker + " is" + " " + result);
                break;
            default:
                System.out.println("Fruit is not in stock");
        }

        ArrayList<String> friends = new ArrayList<>(Arrays.asList("Marcuz","Bryan","Isaiah","Vinz","Luis","Marvic"));

        System.out.println("My friends are: " + friends);

        HashMap<String,String> inventory = new HashMap<>();

        inventory.put("toothpaste", "15");
        inventory.put("toothbrush", "20");
        inventory.put("soap", "12");

        System.out.println("Our inventory consist of: " + inventory);



    }
}